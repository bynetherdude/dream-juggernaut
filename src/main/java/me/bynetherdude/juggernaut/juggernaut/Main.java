package me.bynetherdude.juggernaut.juggernaut;

import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

    public static String prefix = "§7[§aJuggernaut§7] §f";

    @Override
    public void onEnable() {
        System.out.println(prefix + "Juggernaut has been activated!");
    }
}
